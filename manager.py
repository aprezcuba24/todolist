import app.handlers
from app.main import updater

if __name__ == "__main__":
    updater.bot.set_my_commands(updater.dispatcher.my_commands)
    updater.start_polling()
