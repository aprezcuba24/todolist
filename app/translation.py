import gettext

lang_translations = gettext.translation(
    "messages", localedir="locales", languages=["es"]
)
lang_translations.install()

trans = lang_translations.gettext
