from telegram import Update
from telegram.ext import Dispatcher as BaseDispatcher
from app.models import session, User


class Dispatcher(BaseDispatcher):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._my_commands = []

    def process_update(self, update: Update, *args, **kwargs) -> None:
        user = update.effective_user
        if user:
            instance = (
                session.query(User).filter_by(teletram_id=str(user.id)).one_or_none()
            )
            if not instance:
                instance = User(teletram_id=str(user.id))
                session.add(instance)
                session.commit()
        return super().process_update(update, *args, **kwargs)

    def add_command(self, command, description):
        self._my_commands += [(command, description)]

    @property
    def my_commands(self):
        return self._my_commands
