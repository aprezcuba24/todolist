from collections import defaultdict
from telegram.ext import BasePersistence
from app.models import session, User


class MyPersister(BasePersistence):
    def get_user_data(self):
        users = session.query(User).all()
        data = {item.teletram_id: item.state for item in users}
        return defaultdict(dict, data)

    def update_user_data(self, user_id, data):
        user: User = session.query(User).filter_by(teletram_id=str(user_id)).one()
        user.state = data
        session.add(user)
        session.commit()

    def flush(self, *args, **kwargs):
        pass

    def get_bot_data(self):
        pass

    def get_chat_data(self):
        pass

    def get_conversations(self, name):
        users = session.query(User).all()
        data = defaultdict(dict)
        for user in users:
            user_id = int(user.teletram_id)
            conversation = user.conversation_state.get(name, {})
            for chat_id, value in conversation.items():
                data[(chat_id, user_id)] = value
        return data

    def update_bot_data(self):
        pass

    def update_chat_data(self):
        pass

    def update_conversation(self, name, key, new_state):
        chat_id, user_id = key
        user: User = session.query(User).filter_by(teletram_id=str(user_id)).one()
        data = user.conversation_state.get(name, {})
        data[chat_id] = new_state
        user.conversation_state = data
        session.add(user)
        session.commit()
