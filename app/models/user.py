from sqlalchemy import Column, Integer, String, JSON
from sqlalchemy.orm import relationship
from .base import Base


class User(Base):
    __tablename__ = "User"
    id = Column(Integer, primary_key=True)
    teletram_id = Column(String)
    state = Column(JSON)
    conversation_state = Column(JSON)
    tasks = relationship("Task", backref="user", lazy="dynamic")

    def __init__(self, teletram_id=None, state=None, conversation_state=None):
        self.teletram_id = teletram_id
        self.conversation_state = conversation_state if conversation_state else {}
        self.state = state if state else {}
