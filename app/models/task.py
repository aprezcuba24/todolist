from sqlalchemy import Column, Integer, String, Text, ForeignKey
from .base import Base


class Task(Base):
    __tablename__ = "Task"
    id = Column(Integer, primary_key=True)
    title = Column(String)
    description = Column(Text)
    user_id = Column(Integer, ForeignKey("User.id"))

    def __init__(self, title=None, description=None):
        self.title = title
        self.description = description
