from queue import Queue
from telegram.ext.jobqueue import JobQueue
from telegram.utils.request import Request
from telegram.bot import Bot
from telegram.ext import Updater
from . import logging
from app.core.dispatcher import Dispatcher
from app.core.my_persister import MyPersister
from .config import TOKEN


my_persister = MyPersister(store_chat_data=False, store_bot_data=False)
bot = Bot(TOKEN, request=Request(con_pool_size=10))
dispatcher = Dispatcher(
    bot=bot,
    persistence=my_persister,
    update_queue=Queue(),
    job_queue=JobQueue(),
    workers=4,
)
updater = Updater(dispatcher=dispatcher, workers=None)
dispatcher = updater.dispatcher
