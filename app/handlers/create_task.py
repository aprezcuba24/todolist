from telegram.update import Update
from telegram.ext import (
    CallbackContext,
    ConversationHandler,
    CommandHandler,
    MessageHandler,
    Filters,
)
from app.models.task import Task
from app.models import User, session
from app.translation import trans as _
from app.main import dispatcher


NAME, DESCRIPTION = range(2)


def start(update: Update, context: CallbackContext) -> int:
    update.message.reply_text(_("create_task_name"))
    return NAME


def save_name(update: Update, context: CallbackContext):
    text = update.message.text.lower()
    context.user_data["create_task"] = {"title": text.lower()}
    update.message.reply_text(_("create_task_ask_description"))
    return DESCRIPTION


def save_description(update: Update, context: CallbackContext):
    text = update.message.text.lower()
    data = context.user_data["create_task"]
    context.user_data["create_task"] = {**data, **{"description": text.lower()}}
    user_id = update.effective_user.id
    user: User = session.query(User).filter_by(teletram_id=str(user_id)).one()
    task = Task(**context.user_data["create_task"])
    user.tasks.append(task)
    session.commit()
    update.message.reply_text(_("create_task_end"))


conv_handler = ConversationHandler(
    entry_points=[CommandHandler("create_task", start)],
    states={
        NAME: [
            MessageHandler(Filters.all, save_name),
        ],
        DESCRIPTION: [
            MessageHandler(Filters.all, save_description),
        ],
    },
    fallbacks=[],
    name="create_task",
    persistent=True,
)

dispatcher.add_handler(conv_handler)
dispatcher.add_command("/create_task", _("create_task_description"))
