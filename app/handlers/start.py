from telegram import Update
from telegram.ext import CallbackContext
from telegram.ext.commandhandler import CommandHandler
from app.translation import trans as _
from app.main import dispatcher


def start(update: Update, context: CallbackContext) -> None:
    name = " ".join(context.args)
    update.message.reply_text(_("start_message") % name)


dispatcher.add_handler(CommandHandler("start", start))
dispatcher.add_command("/start", _("start_description"))
