import app.models
from app.models.base import Base, engine

Base.metadata.create_all(engine)
